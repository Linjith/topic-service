package com.tekcapsule.topic.application.config;

public class AppConstants {
    private AppConstants() {

    }

    public static final String HTTP_STATUS_CODE_HEADER = "statuscode";
}
